﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Unit1_TAP
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private string serviceUrl = "http://yottaclass.azurewebsites.net/";

        private async void button1_Click(object sender, EventArgs e)
        {
            WebClient request = new WebClient();
            label1.Text = "call service";
            var result=await request.DownloadDataTaskAsync(Path.Combine(serviceUrl, "Home", "GetTextFile"));
            label1.Text = "done";
            
        }

        private Task<byte[]> ToTAPAsync()
        {
            WebClient request = new WebClient();
            TaskCompletionSource<byte[]> source = new TaskCompletionSource<byte[]>();
            request.DownloadDataCompleted += delegate (object sender, DownloadDataCompletedEventArgs e)
            {
                source.SetResult(e.Result);
            };
            request.DownloadDataAsync(new Uri(Path.Combine(serviceUrl, "Home", "GetTextFile")));
            return source.Task;
        }

        private async void button2_Click(object sender, EventArgs e)
        {
            //將unit1_EAP中的button3_Clicke改寫成async await方式並顯示最後結果在label上
            var result = await ToTAPAsync();
            label1.Text = result.ToString();
        }


    }
}
